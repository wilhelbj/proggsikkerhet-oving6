// @flow
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type User = {
  userName: string,
  pwd_hashed: string,
  pwd_hashed_server: string,
  id: number,
};

class authService {
  send(pwd: string, userName: string) {
    return axios
      .post<>('/user', { pwd: pwd, userName: userName })
      .then((response) => response.data);
  }
}

export default new authService();
