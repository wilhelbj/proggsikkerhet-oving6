// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { Card, Row, Column, Form, Button } from './widgets';
import authService, { type User } from './task-service';
import CryptoJS from 'crypto-js';

class LogInn extends Component {
  user: User = [];
  page: number = 0;
  render() {
    if (this.page == 0)
      return (
        <Card title="Log Inn">
          <Row>
            <Column width={1}>
              <Form.Label>userName:</Form.Label>
            </Column>
            <Column width={4}>
              <Form.Input
                type="text"
                value={this.userName}
                onChange={(event) => (this.userName = event.currentTarget.value)}
              ></Form.Input>
            </Column>
          </Row>
          <Row>
            <Column width={1}>
              <Form.Label>Password:</Form.Label>
            </Column>
            <Column width={4}>
              <Form.Input
                type="password"
                value={this.pwd}
                onChange={(event) => (this.pwd = event.currentTarget.value)}
              ></Form.Input>
            </Column>
          </Row>
          <Button.Success
            onClick={() => {
              let resp = this.hash_things(this.pwd, this.userName);
              authService.send(resp, this.userName).then((response) => {
                if (response.auth == 1) {
                  this.page = 1;
                } else if (response.auth == 0) {
                  console.log(response);
                  alert('wrong password for this user');
                } else if (response.auth == -1) {
                  console.log(response);
                  alert('Incorrect userName');
                }
              });
            }}
          >
            Log inn
          </Button.Success>
        </Card>
      );
    else if (this.page == 1) {
      return (
        <Card title="User Info">
          <Row>
            <Column>You are logged in as</Column>
            <Column>{this.userName}</Column>
          </Row>
        </Card>
      );
    }
  }
  hash_things(pwd, userName) {
    var key512Bits = CryptoJS.PBKDF2(pwd, userName, { keySize: 512 / 32 });
    console.log(key512Bits.toString());
    return key512Bits.toString();
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <>
      <LogInn />
    </>,
    root
  );
